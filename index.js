const apibase = 'https://pigherding-api-n2ovv675qq-uc.a.run.app';
const submit = document.querySelector('#submit');
const modal = document.querySelector('.modal');
const thankyou = document.querySelector('.thankyou-modal');

function titleCase (text) {
  return text.split(' ').map(word => word[0].toUpperCase() + word.slice(1)).join(' ');
}

function toBase64 (file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function getTime() {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  const hours = date.getHours();
  const minutes = date.getMinutes();
  return month + '/' + day + '/' + year + ' ' + hours + ':' + ('0' + minutes).slice(-2);
}

function asyncGetCurrentPosition (options) {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject, options);
  });
}

async function fetchLocation(latitude, longitude) {
  try {
    const url = apibase + '/addresses/' + latitude + '/' + longitude;
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
  }
}

async function submitForm(data) {
  try {
    const url = apibase + '/report';
    const response = await fetch(
      url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          report: data
        })
      }
    );
    const { report } = await response.json();
    return report;
  } catch (error) {
    console.error(error);
  }
}

async function main () {
  const timeInput = document.querySelector('#time');
  const cityInput = document.querySelector('#city');
  const locationInput = document.querySelector('#location');
  const { coords: { latitude, longitude } } = await asyncGetCurrentPosition();  
  const { address, city } = latitude && longitude ? await fetchLocation(latitude, longitude) : {address: '', city: ''};

  timeInput.value = getTime();
  cityInput.value = city;
  locationInput.value = address;
}

submit.addEventListener('click', async (event) => {
  event.preventDefault();
  submit.disabled = true;
  modal.style.display = 'flex';

  const footage = Array.from(document.querySelector('#footage').files);
  let files = [];

  for (index in footage) {
    const file = await toBase64(footage[index]);
    files.push(file);
  }
  
  const badges = document.querySelector('#badge-numbers').value;
  const description = document.querySelector('#description').value;
  const time = document.querySelector('#time').value;
  const city = document.querySelector('#city').value;
  const address = document.querySelector('#location').value;
  const data = {
    files,
    badges,
    description,
    time,
    city,
    address
  };

  await submitForm(data);
  
  submit.disabled = false;
  modal.style.display = 'none';
  thankyou.style.display = 'flex';
});


thankyou.addEventListener('click', () => {
  thankyou.style.display = 'none';
  document.querySelector('#badge-numbers').value = '';
  document.querySelector('#description').value = '';
  document.querySelector('#time').value = '';
  document.querySelector('#city').value = '';
  document.querySelector('#location').value = '';
  document.querySelector('#footage').value = '';
  main();
});

main();
